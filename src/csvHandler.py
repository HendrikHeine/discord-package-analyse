import csv


class CSV:
    def __init__(self) -> None:
        pass

    def parse(self, file_name:str):
        payments = []
        with open(file=file_name, mode="r", newline="") as file:
            content = csv.reader(file, delimiter=',', quotechar='|')
            count = 0
            messages = []

            for row in content:
                if count == 0:
                    pass
                else:
                    messages.append(row)
                count += 1
            
            file.close()
            return messages

