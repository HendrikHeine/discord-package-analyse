import json
import os

from models.guild import Guild
from models.message import Message
from models.channel import Channel
from csvHandler import CSV


class Analyse:
    def __init__(self, package_path:str) -> None:
        self.csv = CSV()
        self.package_path = package_path
        self.guild_list = []

    def list_folders(self, path:str):
        return next(os.walk(f"{self.package_path}/{path}"))[1]

    def guilds(self):
        folders = self.list_folders(path="servers")
        for folder in folders:
            with open(file=f"{self.package_path}/servers/{folder}/guild.json") as file:
                guild_json = json.load(file)
            self.guild_list.append(Guild(data=guild_json))

    def channels(self):
        folders = self.list_folders(path="messages")
        for folder in folders:
            with open(file=f"{self.package_path}/messages/{folder}/channel.json") as file:
                channel_json = json.load(file)
            print(folder)
            print(Channel(data=channel_json).name)

    def messages(self):
        pass

    def users(self):
        pass

    def my_account(self):
        pass


