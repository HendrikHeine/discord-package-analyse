class Message:
    def __init__(self, data) -> None:
        self.id = data[0]
        self.timeStamp = data[1]
        self.content = data[2]
        self.attachments = data[3]