class Guild:
    def __init__(self, data:dict) -> None:
        self.id:str = data['id']
        self.name:str = data['name']
        self.channels = []
