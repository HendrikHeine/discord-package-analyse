class Channel:
    def __init__(self, data:dict) -> None:
        self.id:str = data['id']
        self.type:str = data['type']
        self.name:str = data['name']
        self.guild_id:str = data['guild']['id']
        self.guild_name:str = data['guild']['name']


