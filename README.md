# Discord Package Analyse

This project is for analyze the Data from a Discord Data Dump.

## Run

Request the Data from your discord account and paste the `package.zip` file in this project folder. Run main.py to fill the Database and
analyze the data

## Request Data Dump

In the Discord Desktop App:
User Settings > Privacy & Saftey > Request all of my data

## Disclaimer

Please be careful with the downloaded data. In can contain informations about your paiments, telehphonenumber, email, address... <br>
This programm will NOT upload any data to a server. It runs without an internetconnection.

This Programm will only save the message, channel, user and guild IDs and timestamps in the database, to make statistics, based on them.